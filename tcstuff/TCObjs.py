import os
import traceback

# first-party
from tcstuff.app_lib import AppLib

class TCObj:

    def __init__(self, identifier):
        self.identifier = identifier
        app_lib = AppLib()
        app_lib.update_path()

        # import modules after path has been updated
        # third-party
        from tcex import TcEx  # pylint: disable=import-outside-toplevel

        # first-party
        from tcstuff.app import App  # pylint: disable=import-outside-toplevel

        config_file = 'C:\\Users\\BrianTackett\\Documents\\Work\\Code\\VTStuff\\Code\\tcstuff\\app_config.json'
        if not os.path.isfile(config_file):
            print(f'Missing {config_file} config file.')

        tcex = TcEx(config_file=config_file)

        try:
            # load App class
            self.app = App(tcex)

            # perform prep/setup operations
            self.app.setup()

            print('[-] Object successfully initialized')
        except Exception as e:
            print('[x] Exception in object construction: '+str(e))
class TCHash(TCObj):

    def __init__(self, hash):
        TCObj.__init__(self, hash)
        print('[-] TCHash successfully initialized')
        
        self.weblinks = self.app.inTC(hash,'hash')