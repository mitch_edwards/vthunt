"""ThreatConnect Job App"""
# standard library
import csv,json

# first-party
from tcstuff.external_app import ExternalApp  # Import default External App Class (Required)


class App(ExternalApp):
    """External App"""

    def __init__(self, _tcex: object):
        """Initialize class properties."""
        super().__init__(_tcex)
        self.batch = None
        print('[-] Initialized...')

    def inTC(self, indicator, type):
        weblinks = []
        if type == 'hash':
            parameters = {'includes':['additional','attributes','labels','tags']}
            ti = json.loads(self.tcex.session.get(f'/v2/indicators/files/{indicator}/owners').text)
            
            if len(ti['data']['owner']) > 0:
                print('[-] ThreatConnect Links:')
                for owner in ti['data']['owner']:
                    ti = self.tcex.ti.indicator(indicator_type='File', owner=owner['name'], unique_id=indicator)
                    response = ti.single(params=parameters)
                    data = response.json().get('data',{})
                    weblink = data['file']['webLink']
                    #print('\t'+str(weblink))
                    weblinks.append(weblink)
            return weblinks

    def run(self) -> None:
        
        """Run main App logic."""
        try:
            parameters = {'includes':['additional','attributes','labels','tags']}
            ti = self.tcex.ti.indicator(indicator_type='File',owner='Technical Blogs and Reports',unique_id='DAB000FBF17B4C2ED0D6B6009C7BD498301DDCD89AD4B0FD931ED564A6EFF1C4')
            
            print('[-] Running...')
            response = ti.single(params=parameters)
            indicator = response.json().get('data', {})
            print(str(indicator))
        except Exception as e:
            print('[x] Exception: %s' % (str(e)))

        