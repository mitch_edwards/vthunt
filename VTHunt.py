"""
VTHunt.py
USAGE: python3 VTHunt.py --checktc --hash <HASH>
python3 VTHunt.py --checktc --ip <IP>
python3 VTHunt.py --checktc --url <URL>

"""
import sys, vt, json
from tcstuff.TCObjs import TCHash

# import logging
# logging.basicConfig()
try:
    import ConfigParser
except:
    import configparser as ConfigParser

from threatconnect import ThreatConnect

def parseargs(args):    
    parsed_args = {}

    if '--help' in args:
        parsed_args['help'] = True 
    else:
        parsed_args['help'] = False

    if '--checkvt' in args:
        parsed_args['checkVT'] = True
    else:
        parsed_args['checkVT'] = False
    if '--checktc' in args:
        parsed_args['checkTC'] = True 
    else:
        parsed_args['checkTC'] = False 
    if '--hash' in args:
        parsed_args['ishash'] = True 
        parsed_args['isurl'] = False
        parsed_args['isip'] = False
        pos = 0
        while args[pos] != '--hash':
            pos = pos+1
        parsed_args['hash'] = args[pos+1]
    elif '--ip' in args:
        parsed_args['isip'] = True 
        parsed_args['isurl'] = False
        parsed_args['ishash'] = False
        pos = 0
        while args[pos] != '--ip':
            pos = pos+1
        parsed_args['ip'] = args[pos+1]
    elif '--url' in args:
        parsed_args['isurl'] = True 
        parsed_args['ishash'] = False
        parsed_args['isip'] = False
        pos = 0
        while args[pos] != '--url':
            pos = pos+1
        parsed_args['url'] = args[pos+1]
    else:
        parsed_args['isurl'] = False 
        parsed_args['isip'] = False
        parsed_args['ishash'] = False

        parsed_args['help'] = True

    return parsed_args

def getVTAPIKey():
    
    try:
        apifile_r = open('./vtapi.txt','r')
        apikey = apifile_r.readline().strip('\n')
    except:
        print('[x] VirusTotal API Key File Not Found')
        apikey = input('[-] Enter VirusTotal API Key Here: ')
        yesno = input('[-] Would you like to save to file? [y/n]: ')
        if yesno.lower() == 'y':
            apifile_w = open('./vtapi.txt', 'w+')
            apifile_w.write(apikey)
            apifile_w.close()
            print('[-] File saved to \'./vtapi.txt\'')
            
        elif yesno.lower() != 'n':
            print('[x] Invalid input!')
            getVTAPIKey()
    return apikey


def generateVTReport_hash(sample):
    report = 'Report:\n\n'
    vtlink = 'https://virustotal.com/gui/file/'+sample.sha256


    analysis = json.loads(str(sample.last_analysis_stats).replace('\'','"'))
    malicious = analysis['malicious']
    undetected = analysis['undetected']
    suspicious = analysis['suspicious']
    harmless = analysis['harmless']
    filesize = sample.size 
    typeguess = sample.type_tag
    md5 = sample.md5
    sha256 = sample.sha256 
    sha1 = sample.sha1

    report+='VirusTotal Link: '+vtlink

    report+='\nHashes:'
    report+='\n\tmd5: '+md5 
    report+='\n\tsha256: '+sha256 
    report+='\n\tsha1: '+sha1 

    report+='\n\nFile size: '+str(filesize)
    report+='\nFile type (VirusTotal): '+typeguess

    report+='\n\nReport Details:'
    report+='\n\tMalicious: '+str(malicious)
    report+='\n\tSuspicious: '+str(suspicious)
    report+='\n\tUndetected: '+str(undetected)
    report+='\n\tHarmless: '+str(harmless)

    return report

def generateTCReport_hash(hash):
    report = ''
    if len(hash.weblinks) > 0:
        for weblink in hash.weblinks:
            report+='\t'+weblink+'\n'
    else:
        report+='[x] Hash not found in ThreatConnect'

    return report    

def checkVTHash(hash):
    
    try:
        apikey = getVTAPIKey()
        client = vt.Client(apikey)
        sample = client.get_object('/files/'+hash)
        
        report = generateVTReport_hash(sample)
        print(report)
        
    except Exception as e:
        if 'NotFoundError' in str(e):
            print('[x] Could not find specified file hash')
        else:
            print(str(e))
    client.close()
    return {}

def checkTCHash(hash):
    #TODO
    tchash = TCHash(hash)
    try:
        
        report = generateTCReport_hash(tchash)
        print(report)
        
    except Exception as e:
        if 'NotFoundError' in str(e):
            print('[x] Could not find specified file hash')
        else:
            print(str(e))
    return {}

def checkTCIp(ip):
    #TODO
    return {}
def checkVTIp(ip):
    #TODO
    return {}

def checkTCURL(url):
    #TODO
    return {}
def checkVTURL(url):
    #TODO
    return {}

def helpmsg():
    msg = 'VTHunt v0.1\n\n'
    msg += 'Usage:\n'+sys.argv[0]+' [INDICATOR TYPE] [INDICATOR VALUE] [OPTIONS]'

    msg+= '\n\n[INDICATOR TYPE]\n'
    msg+= '--hash\t\t\t\tA SHA256 hash value (more hash types coming soon)'
    msg+='\n\n[OPTIONS]\n'
    msg+='--checkvt\t\t\tCheck VirusTotal for the presence of the indicator.\n'
    msg+='--checktc\t\t\tCheck ThreatConnect for the presence of the indicator.\n'
    msg+='--help\t\t\t\tDisplay this help message\n'
    
    return msg


def main():
    args_parsed = parseargs(sys.argv)
    if args_parsed['help']:
        print(helpmsg())
        return
    if args_parsed['checkVT']:
        if args_parsed['ishash']:
            checkVTHash(args_parsed['hash'])
        if args_parsed['isurl']:
            checkVTURL(args_parsed['url'])
        if args_parsed['isip']:
            checkVTIp(args_parsed['ip'])
        
    if args_parsed['checkTC']:
        if args_parsed['ishash']:
            checkTCHash(args_parsed['hash'])
        if args_parsed['isurl']:
            checkTCURL(args_parsed['url'])
        if args_parsed['isip']:
            checkTCIp(args_parsed['ip'])
if __name__ == "__main__":
    main()